# GRAVITEE-NODEJS-AUTH-PROMPT

## What is it

gravitee-nodejs-auth-prompt is npm module that allow you using prompt to connect using gravitee.

## How to use

### To import the module

```
import { checkAuth, getAuthorizationToken } from '@obelghiti/gravitee-nodejs-auth-prompt';
or
import { checkAuth, getAuthorizationToken } = require('@obelghiti/gravitee-nodejs-auth-prompt');
```

### To initialize the configuration

scope is optional bu default is `openid email roles`

```
return checkAuth({
  baseUrl: (GRAVITEE_GATEWAY_URL),
  clientDomaine: (GRAVITEE_CLIENT_DOMAINE),
  clientId: (GRAVITEE_CLIENT_ID),
  clientSecret: (GRAVITEE_CLIENT_SECRET)
}, scope).then(authStatus => {
  // here you are authenticated
}).catch(({ response: { data }}) => {
  // here theres is authentication error
  console.error(`Authentication error: ${data.error_description}`);
});
```

Explanation of variables:

GRAVITEE_GATEWAY_URL=(url of you gateway)

GRAVITEE_CLIENT_DOMAINE=(domaine of your client)

GRAVITEE_CLIENT_ID=(the client id)

GRAVITEE_CLIENT_SECRET=(the client secret)
